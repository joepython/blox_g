
import os
from pathlib import (Path,
                     PurePath,
                     )

from flask import (Flask,
                   )

from xdb.util import (dev_mode_setup,
                      prod_mode_setup,
                      )


def create_app(test_config=None):

    blx = Flask(__name__, instance_relative_config=True)
    blx.config['USER_HOME'] = str(Path.home())
    blx.config['RUN_MODE'] = 'PROD'
    blx.config.from_object('config')
    if os.path.isdir(blx.instance_path):
        blx.config['RUN_MODE'] = 'DEV'
        blx.config.from_pyfile('dev_config.py')
        dev_mode_setup(blx)
    else:
        prod_mode_setup(blx)

    blx.config['DATABASE'] = os.path.join(blx.config['DB_DIR'],
                                          blx.config['DB_FILE'])
    blx.config['FREEZER_BASE'] = os.path.join(blx.config['DB_DIR'], 'targets')

    # register blueprints
    from blx.routes import blg_bp
    blx.register_blueprint(blg_bp)
    return blx
