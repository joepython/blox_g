from blx import create_app

blx = create_app()


if __name__ == '__main__':

    run_port = blx.config['BLX_RUN_PORT']

    print(f'\n\n****** blx_run.py in {blx.config["RUN_MODE"]} mode on port {run_port}. ******')
    print(f'    DB ---> {blx.config["DATABASE"]}\n\n')

    blx.run(port=run_port, debug=True)
