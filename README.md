#BLOX

**BLOX** is the first Python community shared code base project from the **joepython.com** blog site.

The BLOX application is intended as a common jumping off point for creation of examples, tutorials, presentations, and courses for teaching Python application development.

For more detailed information on the current or historical status of the BLOX project, go to [joepython.com](https://joepython.com).

#####MIT LICENSE
BLOX open source software offered under the MIT license.