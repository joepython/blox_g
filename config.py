# BLOX_g initial configuration values for PROD mode.
# These values may be overridden by values in the 'instance' directory

SECRET_KEY    = 'justdevelopmentmode'
PROD_DATA_DIR = 'blox_data'
PROD_DB_FILE  = 'blox.sqlite'

# Frozen-Flask configuration values

FREEZER_DEV_ROOT = '/Users/tipton/Joe_Dev/blox_dev/dev_blox/staging'
FREEZER_PROD_ROOT = '/Users/tipton/Applications/BLOX/targets'

FREEZER_DESTINATION_ROOT = '/Users/tipton/Applications/BLOX/targets'
FREEZER_REMOVE_EXTRA_FILES = False

SCOPE = 'public'

# Default port values

BLX_RUN_PORT_DEFAULT = 8011
XDB_RUN_PORT_DEFAULT = 5011

DB_DIR_DEFAULT = 'Applications/blox_g/db'


# INITIAL port values set to defaults

BLX_RUN_PORT = BLX_RUN_PORT_DEFAULT
XDB_RUN_PORT = XDB_RUN_PORT_DEFAULT

PROD_DB_DIR = DB_DIR_DEFAULT

# Test the instance/config.py OVERRIDE of the default config
OVERRIDED = False
