
# import logging
import os

from flask import (Blueprint,
                   current_app,
                   flash,
                   redirect,
                   render_template,
                   request,
                   session,
                   url_for,
                   )

from .huddle import (BlogSettings,
                     Huddle,
                     Spiel,
                     )
from .util import (split_date,
                   stencil_options,
                   target_options,
                   thread_options,
                   )

spl_bp = Blueprint('spl', __name__)

huddle = Huddle()
bs = BlogSettings()


@spl_bp.route('/show_all_spiels/', methods=['GET', 'POST'])
@spl_bp.route('/set_spiel_context/', methods=['GET', 'POST'])
def show_all_spiels():

    if 'phrase' in request.form:
        session['phrase'] = request.form['phrase']
    elif 'phrase' not in session:
        session['phrase'] = ''
    if 'thread' in request.form:
        session['thread'] = request.form['thread']
    elif 'thread' not in session:
        session['thread'] = 'BLOX'
    if 'target' in request.form:
        session['target'] = request.form['target']
    elif 'target' not in session:
        session['target'] = 'local'
    if 'stencil' in request.form:
        session['stencil'] = request.form['stencil']
    elif 'stencil' not in session:
        session['stencil'] = 'blog-std'

    spiel_list, db_error = huddle.get_selected_spiels(session['phrase'],
                                                      session['thread'],
                                                      session['target'],
                                                      session['stencil'],
                                                      )
    if db_error is not None:
        flash(db_error)

    db_result = bs.get_settings()

    return render_template('/spiel/show_spiels.html',
                           huddle=huddle,
                           spiel_list=spiel_list,
                           tab_title='Spiels',
                           phrase=session['phrase'],
                           thread=session['thread'],
                           target=session['target'],
                           stencil=session['stencil'],
                           thread_options=thread_options,
                           target_options=target_options,
                           stencil_options=stencil_options,
                           settings=bs,
                           )


@spl_bp.route('/new_spiel/', methods=['GET', 'POST'])
def new_spiel():
    """Present a form for entering basic field for a new Spiel()

    """
    spiel = Spiel()
    db_error = bs.get_settings()
    return render_template('spiel/new_spiel.html',
                           spiel=spiel,
                           stencil_options=stencil_options,
                           target_options=target_options,
                           thread_options=thread_options,
                           settings=bs,
                           )


@spl_bp.route('/add_spiel/', methods=['GET', 'POST'])
def add_spiel():
    """Take form input from new_spiel.html and create new Spiel()
       entry in database.

       Returns: None  -->  Some database operation failed
                False -->
    """
    rf = request.form
    spiel = Spiel()

    # Populate Spiel() for adding to database
    spiel.sid       = 0
    spiel.stencil   = rf['stencil']
    spiel.filename  = rf['filename']
    spiel.title     = rf['title']
    spiel.author    = rf['author']
    spiel.post_date = rf['post_date']
    spiel.tab       = rf['tab']
    spiel.thread    = rf['thread']
    spiel.target    = rf['target']
    if not spiel.filename.endswith('.html'):
        spiel.filename += '.html'
        flash(f'Extension ".html" added to file name.<br/>')

    db_error = huddle.make_new_spiel(spiel)
    if db_error is not None:
        flash(db_error)
        return render_template('spiel/new_spiel.html/',
                               spiel=spiel,
                               stencil_options=stencil_options,
                               target_options=target_options,
                               thread_options=thread_options,
                               )
    flash(f'New spiel added: spiel.sid={spiel.sid} "{spiel.post_date}/{spiel.filename}"')
    return redirect(url_for('spl.show_all_spiels'))


@spl_bp.route('/edit_spiel/', methods=['GET', 'POST'])
def edit_spiel():
    if 'sid' in request.form:
        sid = int(request.form['sid'])
    else:
        return redirect(url_for('spl.show_all_spiels',
                                huddle=current_app.huddle))
    return redirect(url_for('spl.edit_named_spiel', sid=sid))


@spl_bp.route('/edit_spiel/<sid>', methods=['GET', 'POST'])
def edit_named_spiel(sid):
    spiel, db_result = huddle.get_spiel_by_sid(sid)
    if db_result is True:
        flash(f'Spiel ID value ({sid}) is not a number.')
        return redirect(url_for('spl.show_all_spiels'))
    if db_result is not None:
        flash(db_result)
        return redirect(url_for('spl.show_all_spiels'))

    # If we come here from a cancelled deletion.
    if request.referrer:
        if 'confirm_delete_spiel' in request.referrer:
            flash(f'Deletion CANCELLED.')

    p_date = split_date(spiel.post_date)

    db_result = bs.get_settings()
    return render_template('spiel/edit_spiel.html',
                           spiel=spiel,
                           p_date=p_date,
                           tab_title='SpielEdit',
                           stencil_options=stencil_options,
                           target_options=target_options,
                           thread_options=thread_options,
                           settings=bs,
                           )


@spl_bp.route('/update_spiel/<sid>/', methods=['GET', 'POST'])
def update_spiel(sid):
    spiel, db_result = huddle.get_spiel_by_sid(sid)
    if db_result is not None:
        flash(db_result)
        return redirect(url_for('spl.edit_named_spiel', sid=sid))
    db_result = False
    rf = request.form
    if (rf['tab']        != spiel.tab or
        rf['title']     != spiel.title or
        rf['author']    != spiel.author or
        rf['stencil']   != spiel.stencil or
        rf['filename']  != spiel.filename or
        rf['thread']    != spiel.thread or
        rf['target']    != spiel.target or
       rf['post_date'] != spiel.post_date):
            spiel.stencil   = rf['stencil']
            spiel.filename  = rf['filename']
            spiel.title     = rf['title']
            spiel.author    = rf['author']
            spiel.post_date = rf['post_date']
            spiel.tab       = rf['tab']
            spiel.thread    = rf['thread']
            spiel.target    = rf['target']
            db_result = huddle.update_spiel(spiel)
    if db_result is None:
        flash(f'Spiel update successful: {spiel.title}.')
    else:
        flash(db_result)
    return redirect(url_for('spl.edit_named_spiel', sid=sid))


@spl_bp.route('/confirm_delete_spiel/<int:sid>/', methods=['GET', 'POST'])
def confirm_delete_spiel(sid):
    spiel, db_result = huddle.get_spiel_by_sid(sid)
    if db_result is not None:
        flash(db_result)
        return redirect(url_for('spl.edit_named_spiel', sid=sid))
    p_date = split_date(spiel.post_date)
    return render_template('spiel/confirm_spiel_delete.html',
                           spiel=spiel,
                           p_date=p_date,
                           tab_title='SpielEdit',
                           target_options=target_options,
                           thread_options=thread_options,)


@spl_bp.route('/delete_spiel/<int:sid>/', methods=['GET', 'POST'])
def delete_spiel(sid):
    spiel, db_result = huddle.delete_spiel(sid)
    if db_result is None:
        flash(f'Spiel deleted: {spiel.post_date} - "{spiel.title}" (#{sid}).')
        return redirect(url_for('spl.show_all_spiels'))
    if db_result is True:
        flash(f'Delete ID (#{sid}) does not match this Spiel ID (#{sid}).')
    elif db_result is False:
        flash(f'Spiel delete failed.  Unknown error.')
    else:
        flash(db_result)
    return redirect(url_for('spl.confirm_delete_spiel', sid=sid))


