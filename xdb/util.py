# Utility elements and functions

import os
import platform
from pathlib import (Path,
                     PurePath,
                     )

# from instance.assemble_remote import together

stencil_options = ['blog-std',
                   'Home',
                   'About',
                   'ToC',
                   'Scope',
                   'all',
                   ]

# TODO: Threads need to be configurable. Perhaps keep in DB.

thread_options = ['Class_1',
                  'Class_2',
                  'Class_3',
                  'Class_4',
                  'Class_5',
                  'Class_6',
                  'all',
                  'none',
                  ]

target_options = ['public',
                  'review',
                  'internal',
                  'local']

stencil_functions = {'blog-std': 'blg.standard_blog_post',
                     'Home': 'blg.blog_index',
                     'About': 'blg.blog_about',
                     'ToC': 'blg.blog_toc',
                     'Scope': 'blg.blog_index',
                     }

# TODO: Thread_TOC functions need to be configurable. Perhaps keep in DB.
#       Single function for all ToC???

thread_toc_functions = {'BLOX': 'blg.blog_toc',
                        'utility': 'blg.blog_toc',
                        '20yrs': 'blg.blog_toc',
                        'Class_1': 'blg.blog_toc',
                        'Class_2': 'blg.blog_toc',
                        }

scope_options = ['public',
                 'review',
                 'internal',
                 'local',
                 ]


def split_date(post_date):
    post_year, post_month, post_day = post_date.split('-')
    path_date = {'post_year': post_year,
                 'post_month': post_month,
                 'post_day': post_day,
                 }
    return path_date


def dev_mode_setup(app):
    """Finalize configuration for DEV mode

       DEV database is in app-level instance/ directory
       DEV database name is b2.sqlite

    """

    app.config['DB_DIR'] = app.instance_path
    app.config['DB_FILE'] = 'b2.sqlite'

    return


def prod_mode_setup(app):
    """Finalize configuration for PROD mode

       PROD database is in directory ~/Applications/blox_g/db/
       PROD database name is blox_g.sqlite

       Empty file /static/css/dev.css, which changes NavBar
       color to distinguish DEV mode from PROD mode.

    """
    app.config['DB_DIR'] = os.path.join(app.config['USER_HOME'], 'Applications/blox_g/db')
    app.config['DB_FILE'] = 'blox_g.sqlite'
    dev_css = os.path.join(app.root_path, 'static/css/dev.css')
    with open(dev_css, 'w') as make_empty:
        make_empty.write('')
    print(f' *_*_*_*_*_*_*_*_ NOTICE: WROTE EMPTY dev.css file _*_*_*_*_*_*_*_*')
    return


def mac_locate_db(app):
    app.config['DB_PATH'] = PurePath(app.config['USER_HOME'], app.config['PROD_DB_DIR'])
    app.config['DB_PATH'] = PurePath(app.config['USER_HOME'], app.config['DB_DIR'])
    app.config['DB_NAME'] = PurePath(app.config['DB_PATH'], 'blox_g.sqlite')
    app.config['DEV_DB_NAME'] = PurePath(app.config['DEV_DB_PATH'], 'b2.sqlite')
    if os.path.isfile(app.config['DB_NAME']):
        print(f'Found PRODUCTION DB = {app.config["DB_NAME"]}')
    else:
        print(f'Did not find {app.config["DB_NAME"]}')
    if os.path.isfile(app.config['DEV_DB_NAME']):
        print(f'Found DEVELOPMENT DB = {app.config["DEV_DB_NAME"]}')
    else:
        print(f'Did not find {app.config["DEV_DB_NAME"]}')
    print(f'app.instance_path=      {app.instance_path}')
    print(f'app.root_path=          {app.root_path}')
    print(f'app.static_url_path=    {app.static_url_path}')
    return


def linux_locate_db(app):
    print(f'Linux DB location not yet implemented ...')
    quit(11)
    return


def windows_locate_db(app):
    print(f'Windows DB location not yet implemented ...')
    quit(11)
    return


def applications_blox_db(app):
    """
    Confirm existence of the production BLOX database.
    """

    return


def set_freezer_config(app):
    return
