
from collections import namedtuple
import datetime
import os

from flask_frozen import Freezer
from blx import create_app
from xdb.huddle import (Huddle,
                        )
from xdb.util import (split_date,
                      stencil_functions,
                      target_options,
                      )

FROZEN_URL = namedtuple('FROZEN_URL', 'func, path, target')
blx = create_app()
freezer = Freezer(blx,
                  with_no_argument_rules=False,
                  log_url_for=False)

huddle = Huddle()


# TODO: Seems peculiar: freeze.py is in xdb app, but uses blx app.

@freezer.register_generator
def blog_url_generator():
    for gen_tup in gen_spiels:
        freezer_base = blx.config['FREEZER_BASE']
        blx.config['FREEZER_DESTINATION'] = os.path.join(freezer_base,
                                                         gen_tup.target)
        blx.config['SCOPE'] = gen_tup.target
        yield gen_tup.func, gen_tup.path


if __name__ == '__main__':
    """ Writes specified spiels as static pages to staging areas

    """
    publish_date = datetime.datetime.utcnow()
    with blx.app_context():

        # Frozen-Flask needs multiple SCOPE values to cover all staging targets
        blx.config['RESET_SCOPE'] = blx.config['SCOPE']

        # Start with all spiels in the database -- to get standard pages
        spiel_list, db_error = huddle.get_all_spiels()
        if db_error is not None:
            print(db_error)
            quit(55)
        stage_spiels = {'public': [], 'review': [],
                        'internal': [], 'local': []
                        }

        # First produce all standard pages
        for spiel in spiel_list:
            spiel_args = {}
            if spiel.stencil == 'blog-std':
                spiel_args = split_date(spiel.post_date)
                spiel_args['post_fname'] = spiel.filename
                spiel_args['title'] = spiel.title
            if spiel.stencil == 'ToC':
                spiel_args = {'thread_filename': spiel.thread + '.html'}

            spiel_func = stencil_functions[spiel.stencil]
            public_tup = FROZEN_URL(spiel_func, spiel_args, 'public')
            review_tup = FROZEN_URL(spiel_func, spiel_args, 'review')
            internal_tup = FROZEN_URL(spiel_func, spiel_args, 'internal')
            local_tup = FROZEN_URL(spiel_func, spiel_args, 'local')
            if spiel.target == 'public':
                stage_spiels['public'].append(FROZEN_URL(spiel_func,
                                                         spiel_args,
                                                         'public')
                                              )
            if spiel.target == 'review' or spiel.target == 'public':
                stage_spiels['review'].append(review_tup)
            if spiel.target == 'internal' or \
                    spiel.target == 'review' or \
                    spiel.target == 'public':
                stage_spiels['internal'].append(internal_tup)
            stage_spiels['local'].append(local_tup)
            spiel.publish_date = publish_date
            db_error = huddle.publish_spiel(spiel)
            if db_error is not None:
                print(f'Spiel update failed: {spiel.title}')
                print(f'    {db_error}')

    # Call Frozen-Flask for each staging area
    for target in target_options:
        gen_spiels = stage_spiels[target]
        freezer.freeze()

    # Reset the non-freezer SCOPE configuration value
    with blx.app_context():
        blx.config['SCOPE'] = blx.config['RESET_SCOPE']
        del blx.config['RESET_SCOPE']

    quit()
