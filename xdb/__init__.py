
import os
from pathlib import (Path,
                     )

from flask import (Flask,
                   )

from xdb.util import (dev_mode_setup,
                      prod_mode_setup,
                      scope_options,
                      )


def create_app():
    xdb = Flask(__name__, instance_relative_config=True)
    xdb.config['USER_HOME'] = str(Path.home())
    xdb.config['RUN_MODE'] = 'PROD'
    xdb.config.from_object('config')
    if os.path.isdir(xdb.instance_path):
        xdb.config['RUN_MODE'] = 'DEV'
        xdb.config.from_pyfile('dev_config.py')
        dev_mode_setup(xdb)
    else:
        prod_mode_setup(xdb)

    xdb.config['DATABASE'] = os.path.join(xdb.config['DB_DIR'],
                                          xdb.config['DB_FILE'])

    # register db commands
    from . import db
    db.init_app(xdb)

    # register blueprints
    from xdb.generate import gen_bp
    xdb.register_blueprint(gen_bp)
    from xdb.other_views import oth_bp
    xdb.register_blueprint(oth_bp)
    from xdb.spiel_views import spl_bp
    xdb.register_blueprint(spl_bp)
    from xdb.stanza_views import stz_bp
    xdb.register_blueprint(stz_bp)
    from blx.routes import blg_bp
    xdb.register_blueprint(blg_bp)

    return xdb
