
# import logging

from flask import (Blueprint,
                   flash,
                   redirect,
                   render_template,
                   request,
                   session,
                   url_for,
                   )

from .huddle import (Huddle,
                     Rhyme,
                     Stanza,
                     )
from .util import (split_date,
                   stencil_options,
                   )

stz_bp = Blueprint('stz', __name__)

huddle = Huddle()


@stz_bp.route('/new_stanza/<sid>/<brand>', methods=['GET', 'POST'])
def new_stanza(sid, brand):
    """Present the user with a form to add a stanza or rhyme to this Spiel

    """
    spiel, db_error = huddle.get_spiel_by_sid(sid)
    if db_error is not None:
        flash(f'Unable to add new {brand}')
        return redirect(url_for('spl.edit_named_spiel', sid=sid))
    if brand == 'rhyme':
        return render_template('rhyme/new_rhyme.html',
                               spiel=spiel,
                               tab_title='NewRhyme')
    return render_template('stanza/new_stanza.html',
                           spiel=spiel,
                           tab_title='NewStanza')


@stz_bp.route('/add_stanza/<sid>/<brand>', methods=['GET', 'POST'])
def add_stanza(sid, brand):
    """Add entered stanza to this Spiel

    """
    spiel, db_error = huddle.get_spiel_by_sid(sid)

    addl_stanza = Stanza()
    if brand == 'rhyme':
        addl_stanza = Rhyme()
    addl_stanza.d_seq = 0
    addl_stanza.re_seq = 0
    addl_stanza.e_seq = int(request.form['seq_1'])
    if not addl_stanza.e_seq:
        addl_stanza.e_seq = 999999
    addl_stanza.part_1 = request.form['part_1_1']
    addl_stanza.part_2 = request.form['part_2_1']
    # Do not add if stanza.body input is null.
    if not addl_stanza.part_2:
        flash(f' Add {brand} abandoned.')
        return redirect(url_for('spl.edit_named_spiel', sid=sid))

    if addl_stanza.brand == 'stanza':
        spiel.stanzas.append(addl_stanza)
    else:
        spiel.rhymes.append(addl_stanza)
    spiel, db_error = huddle.update_stanzas(spiel, brand)
    if db_error is None:
        flash(f'Insert {brand} failed.')
    elif db_error is False:
        flash(f'No {brand} to be added.')
    else:
        flash(f'Insert {brand} successful: item #{addl_stanza.re_seq}.')
    p_date = split_date(spiel.post_date)
    return render_template(f'spiel/edit_spiel.html',
                           spiel=spiel,
                           p_date=p_date,
                           tab_title='SpielEdit',
                           stencil_options=stencil_options
                           )


@stz_bp.route('/plus_stanza/<sid>/<brand>', methods=['GET', 'POST'])
def plus_stanza(sid, brand):
    """Add [placeholder] Rhyme or Stanza to this Spiel

    """
    spiel, db_error = huddle.get_spiel_by_sid(sid)
    if db_error is not None:
        flash(f'Unable to append placeholder {brand}.')
        return redirect(url_for('spl.edit_named_spiel', sid=sid))
    r_seq = 1100 + len(spiel.rhymes) * 100
    addl_stanza = Rhyme()
    addl_stanza.part_1 = 'graph'
    if brand == 'stanza':
        r_seq = 1100 + len(spiel.stanzas) * 100
        addl_stanza = Stanza()
        addl_stanza.part_1 = '[placeholder]'
    addl_stanza.d_seq = 0      # Signals "new entry"
    addl_stanza.re_seq = r_seq
    addl_stanza.e_seq = r_seq
    addl_stanza.part_2 = '[placeholder]'
    if addl_stanza.brand == 'stanza':
        spiel.stanzas.append(addl_stanza)
    else:
        spiel.rhymes.append(addl_stanza)

    spiel, db_error = huddle.update_stanzas(spiel, brand)
    if db_error is None:
        flash(f'Insert of {brand} successful: item #{addl_stanza.re_seq}.')
    elif db_error is False:
        flash('No {brand} to be added.')
    else:
        flash(db_error)
    return redirect(url_for(f'spl.edit_named_spiel', sid=sid))


@stz_bp.route('/edit_all_stanzas/<sid>/<brand>', methods=['GET', 'POST'])
@stz_bp.route('/edit_all_stanzas/<sid>/<brand>/<loc>', methods=['GET', 'POST'])
def edit_all_stanzas(sid, brand, loc=0):
    spiel, db_error = huddle.get_spiel_by_sid(sid)
    if db_error is not None:
        flash(f'Unable to access {brand}s.')
        return redirect(url_for('spl.edit_named_spiel', sid=sid))
    if brand == 'rhyme':
        return render_template('rhyme/edit_rhymes.html',
                               spiel=spiel,
                               rhymes=spiel.rhymes,
                               tab_title='EditRhymes',
                               loc=int(loc))
    return render_template('stanza/edit_stanzas.html',
                           spiel=spiel,
                           stanzas=spiel.stanzas,
                           tab_title='EditStanzas',
                           loc=int(loc))


@stz_bp.route('/update_stanzas/<sid>/<brand>', methods=['GET', 'POST'])
def update_stanzas(sid, brand):

    # First, check for cancellation. Do not process last edits.
    for x in request.form:
        if x == 'cancel':
            flash(f'Last {brand} edits cancelled.')
            return redirect(url_for('spl.edit_named_spiel', sid=sid))

    # Process edits ahead of any other action
    spiel, db_error = huddle.get_spiel_by_sid(sid)
    edited_entries = order_edits(request.form, brand)
    huddle_entries = spiel.stanzas
    if brand == 'rhyme':
        huddle_entries = spiel.rhymes
    for edited_entry in edited_entries:
        for h_entry in huddle_entries:
            if h_entry.d_seq == edited_entry.d_seq:
                h_entry.e_seq = edited_entry.e_seq
                h_entry.part_1 = edited_entry.part_1
                h_entry.part_2 = edited_entry.part_2
                h_entry.edit_date = edited_entry.edit_date

    # Check for 'insertion' or 'deletion' (only one possible)
    for x in request.form:
        if x.startswith('ins_'):
            insert_brand_entry(spiel, brand, x)
        elif x.startswith('delete_'):
            delete_brand_entry(spiel, brand, x)

    spiel, update_return = huddle.update_stanzas(spiel, brand)
    if update_return is False:
        flash(f'No {brand} update required.')
    elif update_return is not None:
        flash(f'Update of {brand}s failed.')
    elif update_return is None:
        flash(f'Update(s) of {brand}s succeeded.')
    else:
        flash(f'Unknown error.  Update failed.')

    # If item-level "Upd" "Insert" or "Delete" button was used,
    # stay on current edit page

    # If indicated, switch to other (Rhyme<-->Stanza) edit page.
    for x in request.form:
        if x.startswith(('update_', 'ins_up_', 'ins_dn_', 'delete_')):
            str_x = x[7:]
            int_x = int(str_x)
            if x.startswith('ins_dn_'):
                int_x += 100
            elif x.startswith('delete_'):
                int_x -= 100
            if int_x < 1100:
                int_x = 1100
            str_x = str(int_x)
            return redirect(url_for('stz.edit_all_stanzas',
                                    sid=sid, brand=brand, loc=str_x))
        if x == 'stanza':
            return redirect(url_for('stz.edit_all_stanzas',
                                    sid=sid, brand='stanza'))
        if x == 'rhyme':
            return redirect(url_for('stz.edit_all_stanzas',
                                    sid=sid, brand='rhyme'))
    return redirect(url_for('spl.edit_named_spiel', sid=sid))


def order_edits(entries, brand):
    """Extract Stanza() attributes from edit_stanzas.html form input

    NOTE: Dependent on <form> definition.
    NOTE: spiel_num included at end of request.form values
    """
    r_list = []
    # TODO: This is not terribly robust - but works for now
    for n in range(1, (len(entries) // 4) + 1):
        seq = f'seq_{n}'
        org = f'orig_seq_{n}'
        pt1 = f'part_1_{n}'
        pt2 = f'part_2_{n}'
        if brand == 'stanza':
            addl_stanza = Stanza()
        else:
            addl_stanza = Rhyme()
        addl_stanza.d_seq  = int(entries[org])
        addl_stanza.e_seq  = int(entries[seq])
        addl_stanza.part_1 = entries[pt1]
        addl_stanza.part_2 = entries[pt2]
        r_list.append(addl_stanza)
    return r_list


def insert_brand_entry(spiel, brand, form_value):
    """Insert placeholder Rhyme or Stanza entry at indicated location

    """
    if brand == 'rhyme':
        rhyme = Rhyme()
        rhyme.spiel_sid = spiel.sid
        rhyme.d_seq = 0
        if form_value.startswith('ins_up_'):
            rhyme.e_seq = int(form_value.replace('ins_up_', '')) - 1
        else:
            rhyme.e_seq = int(form_value.replace('ins_dn_', '')) + 1
        rhyme.part_1 = 'graph'
        rhyme.part_2 = '[placeholder]'
        spiel.rhymes.append(rhyme)
    else:
        stanza = Stanza()
        stanza.spiel_sid = spiel.sid
        stanza.d_seq = 0
        if form_value.startswith('ins_up_'):
            stanza.e_seq = int(form_value.replace('ins_up_', '')) - 1
        else:
            stanza.e_seq = int(form_value.replace('ins_dn_', '')) + 1
        stanza.part_1 = '[placeholder]'
        stanza.part_2 = '[placeholder]'
        spiel.stanzas.append(stanza)
    return


def delete_brand_entry(spiel, brand, form_value):
    """Insert placeholder Rhyme or Stanza entry at indicated location

    """
    delete_seq = int(form_value.replace('delete_', ''))
    if brand == 'rhyme':
        spiel_array = spiel.rhymes
    else:
        spiel_array = spiel.stanzas
    for element in spiel_array:
        if element.d_seq == delete_seq:
            element.e_seq = 0
            spiel_array.append(element)
            break
    return
