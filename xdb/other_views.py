
# import logging

from flask import (Blueprint,
                   current_app,
                   flash,
                   redirect,
                   render_template,
                   request,
                   url_for,
                   )

from .huddle import (BlogSettings,
                     )
from .util import (scope_options,
                   )

oth_bp = Blueprint('oth', __name__)

bs = BlogSettings()


@oth_bp.route('/blox_local/', methods=['GET', 'POST'])
def site_index():
    if 'scope' in request.form:
        current_app.config['SCOPE'] = request.form['scope']
    else:
        current_app.config['SCOPE'] = 'local'
    return render_template('blox_local.html',
                           scope=current_app.config['SCOPE'],
                           scope_options=scope_options)


@oth_bp.route('/settings/', methods=['GET', 'POST'])
def settings_form():
    """
    blog_settings is BlogSettings() object
    db_result is None OR a list of database error messages

    """
    db_result = bs.get_settings()
    if db_result is not None:
        for message in db_result:
            flash(message)
    return render_template('settings.html',
                           blog_settings=bs,
                           )


@oth_bp.route('/settings/update/', methods=['GET', 'POST'])
def settings_update():
    form = request.form
    for x in request.form:
        print(f'form[{x}] = {form[x]}  type={type(form[x])}')
    bs.title = form['blogtitle']
    bs.footer['heading'] = form['footer_heading']
    bs.footer['paragraph'] = form['footer_paragraph']
    bs.copyright['holder'] = form['holder']
    bs.copyright['start_year'] = form['start_year']
    bs.copyright['end_year'] = form['end_year']
    bs.e_threads = set_threads(form, len(bs.e_threads))
    print(f'Edited Threads: {bs.e_threads}')
    db_result = bs.update_settings()
    if db_result is None:
        flash('Global Settings update successful.')
    else:
        flash(str(db_result))
    return redirect(url_for('oth.settings_form'))


@oth_bp.route('/view/', methods=['GET', 'POST'])
def live_view_form():
    return render_template('not_implemented.html',
                           page_name="View")


@oth_bp.route('/publish/', methods=['GET', 'POST'])
def publish_form():
    return render_template('not_implemented.html',
                           page_name="Publish")


def set_threads(form, n):
    # Transfer values into edited threads list
    new_threads = []
    for x in range(1, n + 1):
        i = f'idx_{x}'
        v = f'nav_{x}'
        m = f'tname_{x}'
        new_thread = {'index_order': form[i],
                    'nav_order': form[v],
                    'thread_name': form[m]}
        new_threads.append(new_thread)
    # Then we add any new thread (at most one)
    if form['t_index'].isnumeric() \
       and form['t_nav'].isnumeric() \
       and form['t_name']:
        print(f'Add thread Ind={form["t_index"]}')
        new_thread = {'index_order': int(form["t_index"]),
                      'nav_order': int(form["t_nav"]),
                      'thread_name': form["t_name"]}
        new_threads.append(new_thread)
    # Warn if "additional line" is partially filled in
    elif form['t_index'].isnumeric() or \
         form['t_nav'].isnumeric() or \
         form['t_name']:
        print(f'Must have three valid values to enter new Thread')
    # Finally, we take care of any deletion (at most one)
    for x in range(len(new_threads) + 1):
        if f'delete_{x}' in form:
            print(f'Delete #{x}')
            del new_threads[x - 1]
    return new_threads
