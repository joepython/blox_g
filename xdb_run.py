from xdb import create_app

xdb = create_app()


if __name__ == '__main__':

    run_port = xdb.config['XDB_RUN_PORT']
    print(f'\n\n****** xdb_run.py in {xdb.config["RUN_MODE"]} mode on port {run_port}. ******')
    print(f'    DB ---> {xdb.config["DATABASE"]}\n\n')

    xdb.run(port=run_port, debug=True)
