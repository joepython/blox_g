# hidden configuration values

SECRET_KEY   = 'prod'
ALLOT_1 = 'lpxt#j32jr&stov'
ALLOT_2 = 'sm93o8pqq_hdrcw'

'''
    Check the module assemble_remote.py in the instance directory
    to see how the SSHPASS environment variable is set to allow
    scp access to the web site.
    
'''

DEV_BASE = ''
PROD_BASE = ''

# Test config OVERRIDE from here
OVERRIDED = True


# ---------


BLX_RUN_PORT = 8012
XDB_RUN_PORT = 5012

